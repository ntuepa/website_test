window.addEventListener("load", ()=>{
    /*
        const cover_page = document.getElementsByClassName('covet_page_content')[0];
        let covet_page_delay = window.setTimeout(()=>{cover_page.classList.remove("not_show");},500);
    */
    var menu_vue_controller = new Vue({
        el:"#menu",
        data:{
            menu_status:{"Menu_on":false},
        },
        methods:{
            toggle_menu(){
                if (this.menu_status.Menu_on==true) this.menu_status.Menu_on = false;
                else this.menu_status.Menu_on = true;
            },
            smooth_scroll(event){
                this.toggle_menu();
                let destination_ID = event.target.hash;
                console.log(document.querySelector(destination_ID));
                if (destination_ID!=""){
                    let destination = document.querySelector(destination_ID);
                    event.preventDefault();
                    window.scrollTo({'behavior': 'smooth', 'top': destination.offsetTop});
                    
                }
            }
        }
    });
    var info_cue_controller=new Vue({
        el:"#related_info",
        data:{
            info_content_status:[{'info_item_text_closed':true},{'info_item_text_closed':true},
                                {'info_item_text_closed':true},{'info_item_text_closed':true}]
        },
        methods:{
            toggle_info_content(index){
                for(i=0;i<this.info_content_status.length;i++){
                    if(i!=index) this.info_content_status[i].info_item_text_closed = true;
                    else{
                        if(this.info_content_status[i].info_item_text_closed) this.info_content_status[i].info_item_text_closed = false;
                        else this.info_content_status[i].info_item_text_closed = true;
                    }
                }
            }
        }
    });
    var video_vue_controller=new Vue({
        el:"#videos",
        data:{
            v_list:[
                    {"video_src":"https://www.youtube.com/embed/gswPA6dG0Bo","thumbnail":"./material/thumbnail_以人為本的AI時代.jpg"},
                    {"video_src":"https://www.youtube.com/embed/IOd9X_l9s1E","thumbnail":"./material/thumbnail_科普一傳十.jpg"},
                    {"video_src":"https://www.youtube.com/embed/2GmeY-yyrgw","thumbnail":"./material/thumbnail_知覺與覺知-意識的二重奏.jpg"},
                    {"video_src":"https://www.youtube.com/embed/En3k5bhW-uI?start=3222","thumbnail":"./material/thumbnail_科技部專題研究計畫寫作工作坊.jpg"}
            ],
            current_video:0,
            v_page:0
        },
        computed:{
            v_item_style(){
                let list = [];
                for(i=0;i<this.v_list.length;i++){
                    if (4*this.v_page<=i && i<4+4*this.v_page){
                        if(i==this.current_video) list.push({'not_selected':false, 'selected':true, 'not_on_page':false});
                        else list.push({'not_selected':true, 'selected':false, 'not_on_page':false});
                    }
                    else{
                       list.push({'not_selected':false, 'selected':false, 'not_on_page':true}); 
                    }
                }
                return list;
            },
            arrow_stylle(){
                let style = []
                if(this.v_page==0){
                    if(this.v_list.length>4) style = [{show_arrow:false}, {show_arrow:true}];
                    else style = [{show_arrow:false}, {show_arrow:false}];
                }
                else{
                    if (this.v_page==Math.ceil(this.v_list.length/4)-1) style = [{show_arrow:true}, {show_arrow:false}];
                    else style = [{show_arrow:true}, {show_arrow:true}];
                }
                return style;
            }
        },
        methods:{
            video_delection(index){
                this.current_video = index;
            },
            page_selection(value){
                this.v_page += value
            }
        }
    });
});
